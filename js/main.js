//Task 1

// let twiceArray = [
//     [1, 2, 3, 4, 5],       
//     [5, 6, 7, 8, 9],       
//     [20, 21, 34, 56, 100]
// ];

// const sum = arr => arr.map(item => Math.min(...item))
//     .reduce((accum, currentValue) => accum + currentValue);;
    
// console.log(sum(twiceArray));


//Task 2

// let oldestArray = [1, 2, 8, 10];

// const twoOldestAges = arr => arr.sort((a, b) => a - b).slice(-2);

// console.log(twoOldestAges(oldestArray));


//Task 3

// let text = `Lorem ipsum, dolor sit amet consectetur adipisicing elit.
//             Sit quod deserunt hic sunt illo laudantium molestiae,
//             quibusdam ad iusto velit asperiores assumenda beatae earum non dolorum consectetur commodi harum incidunt?`;

// const conversionText = text => text.split(/\s* \s*/g).sort((a, b) => b.length - a.length).slice(-1).join('').length;

// console.log(conversionText(text));

//Task 4

// let text = 'is2 This1 test4 a3';

// const sortSentence = text => text.match(/[1-9]/g).sort((a, b) => a - b);              

// console.log(sortSentence(text));


//Task 5

// const backgroundColor = document.getElementById('text'),
//       width = document.getElementById('width'),
//       height = document.getElementById('height'),
//       button = document.getElementById('button'),
//       circle = document.querySelector('.circle'),
//       input = document.querySelector('input');

// const getData = data => {
//     circle.style.backgroundColor = `${backgroundColor.value}`;
//     circle.style.width = `${width.value}px`;
//     circle.style.height = `${height.value}px`;
// };

// const changeCircle = event => {
//     if(event) {
//         getData();
//     }
// };

// input.addEventListener('change', getData);
// button.addEventListener('click', changeCircle);

